#!/usr/bin/env python

import time
import json
import requests

TRADE_MAX = 100.0
TRADE_MIN = 10.0


url_time = "https://api.kraken.com/0/public/Time"
url_assets = "https://api.kraken.com/0/public/Assets"
url_assetpairs = "https://api.kraken.com/0/public/AssetPairs"
url_ticker = "https://api.kraken.com/0/public/Ticker"
url_OHLC = "https://api.kraken.com/0/public/OHLC"
url_depth = "https://api.kraken.com/0/public/Depth"
url_trades = "https://api.kraken.com/0/public/Trades"
url_spread = "https://api.kraken.com/0/public/Spread"

class Trader(object):

    def __init__(self):
        self.balance = 0
        self.max_orders = 10
        self.last_price = self.get_market_rate()

    def __str__(self):
        return str(self.balance)

    def __repr__(self):
        return str(self.balance)

    def nr_open_orders(self):
        return 10 #TODO

    def buy(self, amount):
        pass

    def sell(self, amount):
        pass

    def get_market_rate(self):
        payload = {"pair":["XXBTZUSD"] , "count":1}
        r = requests.post(url=url_depth, data=payload)
        highest_bid_price = json.loads(r.text)["result"]["XXBTZUSD"]["bids"][0][0]
        lowest_ask_price = json.loads(r.text)["result"]["XXBTZUSD"]["asks"][0][0]

        return float(lowest_ask_price)

    def mainloop(self):

        while True:
            self.determine_action()
            time.sleep(1.0)


    def determine_action(self):
        """ Determines an action to perform. """

        price = self.get_market_rate()
        print("Current price: %f"%price)

        if price > self.last_price:
            self.sell(10)
        elif price < self.last_price:
            self.buy(10)
        else:
            pass

    def run(self):
        self.mainloop()


def main():
    trader = Trader()
    trader.run()

if __name__ == "__main__":
    main()
    print(vault)


